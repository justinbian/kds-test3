################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../stack/Ethernet.c \
../stack/NetBIOS.c \
../stack/arp.c \
../stack/dhcp.c \
../stack/dns.c \
../stack/ftp.c \
../stack/ftp_client.c \
../stack/http.c \
../stack/icmp.c \
../stack/igmp.c \
../stack/ip.c \
../stack/ip_utils.c \
../stack/mqtt.c \
../stack/pop3.c \
../stack/ppp.c \
../stack/smtp.c \
../stack/snmp.c \
../stack/tcp.c \
../stack/telnet.c \
../stack/tftp.c \
../stack/udp.c \
../stack/webutils.c \
../stack/zero_config.c 

OBJS += \
./stack/Ethernet.o \
./stack/NetBIOS.o \
./stack/arp.o \
./stack/dhcp.o \
./stack/dns.o \
./stack/ftp.o \
./stack/ftp_client.o \
./stack/http.o \
./stack/icmp.o \
./stack/igmp.o \
./stack/ip.o \
./stack/ip_utils.o \
./stack/mqtt.o \
./stack/pop3.o \
./stack/ppp.o \
./stack/smtp.o \
./stack/snmp.o \
./stack/tcp.o \
./stack/telnet.o \
./stack/tftp.o \
./stack/udp.o \
./stack/webutils.o \
./stack/zero_config.o 

C_DEPS += \
./stack/Ethernet.d \
./stack/NetBIOS.d \
./stack/arp.d \
./stack/dhcp.d \
./stack/dns.d \
./stack/ftp.d \
./stack/ftp_client.d \
./stack/http.d \
./stack/icmp.d \
./stack/igmp.d \
./stack/ip.d \
./stack/ip_utils.d \
./stack/mqtt.d \
./stack/pop3.d \
./stack/ppp.d \
./stack/smtp.d \
./stack/snmp.d \
./stack/tcp.d \
./stack/telnet.d \
./stack/tftp.d \
./stack/udp.d \
./stack/webutils.d \
./stack/zero_config.d 


# Each subdirectory must supply rules for building sources it contributes
stack/%.o: ../stack/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g -D_KINETIS -D_KDS -D_GNU -I"C:/Users/bianj/OneDrive/Desktop/uTasker-Kinetis-master/Applications/uTaskerV1.4" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


