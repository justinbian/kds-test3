################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../uTasker/Driver.c \
../uTasker/GlobalTimer.c \
../uTasker/SSC_drv.c \
../uTasker/Tty_drv.c \
../uTasker/USB_drv.c \
../uTasker/Watchdog.c \
../uTasker/can_drv.c \
../uTasker/eth_drv.c \
../uTasker/iic_drv.c \
../uTasker/low_power.c \
../uTasker/time_keeper.c \
../uTasker/uFile.c \
../uTasker/uMalloc.c \
../uTasker/uNetwork.c \
../uTasker/uTasker.c 

OBJS += \
./uTasker/Driver.o \
./uTasker/GlobalTimer.o \
./uTasker/SSC_drv.o \
./uTasker/Tty_drv.o \
./uTasker/USB_drv.o \
./uTasker/Watchdog.o \
./uTasker/can_drv.o \
./uTasker/eth_drv.o \
./uTasker/iic_drv.o \
./uTasker/low_power.o \
./uTasker/time_keeper.o \
./uTasker/uFile.o \
./uTasker/uMalloc.o \
./uTasker/uNetwork.o \
./uTasker/uTasker.o 

C_DEPS += \
./uTasker/Driver.d \
./uTasker/GlobalTimer.d \
./uTasker/SSC_drv.d \
./uTasker/Tty_drv.d \
./uTasker/USB_drv.d \
./uTasker/Watchdog.d \
./uTasker/can_drv.d \
./uTasker/eth_drv.d \
./uTasker/iic_drv.d \
./uTasker/low_power.d \
./uTasker/time_keeper.d \
./uTasker/uFile.d \
./uTasker/uMalloc.d \
./uTasker/uNetwork.d \
./uTasker/uTasker.d 


# Each subdirectory must supply rules for building sources it contributes
uTasker/%.o: ../uTasker/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g -D_KINETIS -D_KDS -D_GNU -I"C:/Users/bianj/OneDrive/Desktop/uTasker-Kinetis-master/Applications/uTaskerV1.4" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


